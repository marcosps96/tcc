f = read('maxheightsmap', -1,3);

//scf(1);

[lines columns] = size(f);

x = f(1:89105, 1);
y = f(1:89105, 2);
z = f(1:89105, 3);

heightmap = zeros(401,401);

for i = 1:lines
    x = f(i,1);
    y = f(i,2);
    if f(i,3) == -100000 then
        heightmap(x+200,y+200) = 0; 
    else
        heightmap(x+200,y+200) = f(i,3);
    end
    
    
    
end

a = scf();
plot3d1(heightmap);
a.color_map = jetcolormap(5);


//surf(heightmap,'edgecol','blu');
//for i = 1:89105
    //x = f(i,1)
    //y = f(i,2)
    //z = f(i,3)
    
    //surf(x,y,z)
//end
