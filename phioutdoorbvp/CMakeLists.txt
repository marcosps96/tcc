cmake_minimum_required(VERSION 2.8.3)
project(phioutdoorbvp)

find_package(
  catkin REQUIRED COMPONENTS 
  roscpp
  laser_geometry
  tf
#  geometry_msgs 
#  sensor_msgs
#  std_msgs
#  robot_state_publisher
#  urdf
)

find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)

link_directories(
  ${catkin_LIBRARY_DIRS}
  ${OPENGL_LIBRARIES}
  ${GLUT_LIBRARIES}
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${OPENGL_INCLUDE_DIRS}
  ${GLUT_INCLUDE_DIRS}
)

catkin_package(
#    DEPENDS openni_camera openni_tracker
#    CATKIN DEPENDS message_runtime
#    rospy
#    roscpp
#    tf
#    visualization_msgs
#    std_msgs
#    geometry_msgs
)

add_executable(program
  src/main.cpp
  src/Utils.cpp
  src/GlutClass.cpp
  src/Grid.cpp
  src/Robot.cpp
  src/P3ATRobot.cpp
  src/Potential.cpp
)

target_link_libraries(program
  ${catkin_LIBRARIES}
  ${OPENGL_LIBRARIES}
  ${GLUT_LIBRARIES}
  pthread
  freeimage
)

