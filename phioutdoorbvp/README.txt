Muda pra branch correta:
 git checkout ros_p3at

Antes de tudo (só na 1ª vez), compila o codigo pelo terminal:
catkin_make

Como rodar o codigo no QtCreator:
1. Abre qtcreator via terminal, de dentro do workspace
2. Abre o projeto CMakeLists.txt

Na primeira vez no computador, é preciso configurar o projeto no QtCreator:
1. Seta o caminho do executavel para:
/home/renan/Pioneer_workspace/build/phioutdoorbvp 

Se quiser habilitar o DEBUG do QtCreator:
1. Vai em Projects->Build (Build Settings)
2. Coloca em Cmake arguments: 
-DCMAKE_BUILD_TYPE=Release
3. Vai em Edit build configuration->Rename e renomeia 'Default' para 'Release'
4. Vai em Edit build configuration->Add->Clone Selected e dá o nome 'Debug'
5. Coloca em Cmake arguments: 
-DCMAKE_BUILD_TYPE=Debug
6. Vai em Projects->Run (Run Settings)
7. Vai em Run configuration->Add->Custom Executable
8. Coloca no campo Executable: 
%{buildDir}/devel/lib/phioutdoorbvp/program
9. Vai em Run configuration->Rename e renomeia para 'Run program'

Pra rodar o simulador:
roslaunch p3at_phi_framework example-park.launch


rodar no terminal antes do simulador
$ export LC_NUMERIC=C