#include <cstdio>
#include <GL/glut.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>


using namespace std;

#include "Grid.h"
#include "math.h"
#include "P3ATRobot.h"

Grid::Grid ()
{
    mapScale_ = 10; // 10 células por metro (10cm cada célula)
    mapWidth_ = mapHeight_ = 2000; // 2000 células (200m)
    numCellsInRow_ = mapWidth_;
    halfNumCellsInRow_ = mapWidth_/2;


    cells_ = new Cell[mapWidth_ * mapHeight_];

    for (unsigned int j = 0; j < numCellsInRow_; ++j)
    {
        for (unsigned int i = 0; i < numCellsInRow_; ++i)
        {
            unsigned int c = j*numCellsInRow_ + i;
            cells_[c].x = -halfNumCellsInRow_ + 1 + i;
            cells_[c].y =  halfNumCellsInRow_ - j;

            cells_[c].visited = false;
            cells_[c].free = true;
            cells_[c].checked = false;
            cells_[c].isObstacle = false;
            cells_[c].isObstaclelow = false;
            cells_[c].nearObstacle = false;
            cells_[c].nearObstaclelow = false;
            cells_[c].fakeObstacle = false;
            cells_[c].potential = 0.0;
            cells_[c].gradNorm = 0.0;
            cells_[c].preference = 0.0;
            cells_[c].himm = 0;
            cells_[c].himm_count = 0;
            cells_[c].laser_1_count = 0;
            cells_[c].laser_2_count = 0;
            cells_[c].height = UNDEF;
            cells_[c].heightMin = UNDEF;
            cells_[c].heightMax = UNDEF;
            cells_[c].heightMinLaser = UNDEF;

            cells_[c].distWalls = 0.0;
            cells_[c].dirX = 0.0;
            cells_[c].dirY = 0.0;
        }
    }

    laser_count=0; //keeping the global count of HIMM synchronous
    himm_count=0;

    numViewModes=7;
    viewMode=0;

    showValues=false;
    showArrows=false;

    minHeight = 10000;
    maxHeight = UNDEF;
}

Cell* Grid::getCell (int x, int y)
{
    int i = x + halfNumCellsInRow_ - 1;
    int j = halfNumCellsInRow_ - y;
    return &(cells_[j*numCellsInRow_ + i]);
}

void Grid::createFiles(){

    //create height map file

    ofstream max_height, min_height, height;
    max_height.open ("maxheightmap");
    min_height.open ("minheightmap");
    height.open ("heightmap");

    for (unsigned int j = 0; j < numCellsInRow_; ++j)
        for (unsigned int i = 0; i < numCellsInRow_; ++i){
            unsigned int c = j*numCellsInRow_ + i;

            int x = -halfNumCellsInRow_ + 1 + i;
            int y =  halfNumCellsInRow_ - j;

            if( (x > -200 && x < 200) && (y > -200 && y < 200)){
                max_height << x << " " << y << " " << cells_[c].heightMax << endl;
                min_height << x << " " << y << " " << cells_[c].heightMin << endl;
                height << x << " " << y << " " << cells_[c].height << endl;            
            }
        }

    max_height.close();
    min_height.close();
    height.close();
    return;

}

int Grid::getMapScale()
{
    return mapScale_;
}

int Grid::getMapWidth()
{
    return mapWidth_;
}

int Grid::getMapHeight()
{
    return mapHeight_;
}

void Grid::draw(int xi, int yi, int xf, int yf)
{
    glLoadIdentity();
    Cell *c;

    for(int i=xi; i<=xf; ++i){
        for(int j=yi; j<=yf; ++j){
            drawCell(i+j*numCellsInRow_);
        }
    }

    if(showArrows){
        glPointSize(2);
        for(int i=xi; i<=xf; ++i){
            for(int j=yi; j<=yf; ++j){
                unsigned int c = j*numCellsInRow_ + i;
                if(cells_[c].visited)
                    drawVector(i+j*numCellsInRow_);
            }
        }
    }

    if(showValues){
        for(int i=xi; i<=xf; i++){
            for(int j=yi; j<=yf; j++){
                unsigned int c = j*numCellsInRow_ + i;
                if(cells_[c].visited)
                    drawText(i+j*numCellsInRow_);
            }
        }
    }

}

void Grid::drawCell(unsigned int n)
{
    float aux=0.0;

    if(viewMode==0){
        aux=(16.0-cells_[n].himm)/16.0;
        glColor3f(aux,aux,aux);

    }else if(viewMode==1){
        if(cells_[n].isObstacle)
            glColor3f(0.0,0.0,0.0);
        else if(cells_[n].isObstaclelow)
            glColor3f(0.3, 0.3, 0.3);
        else if(cells_[n].nearObstaclelow)
            glColor3f(1.0,0.0,0.0);
        else if(cells_[n].nearObstacle)
            glColor3f(0.0,0.0,1.0);   
        else if(cells_[n].visited)
            glColor3f(1.0,1.0,1.0);     
        else
            glColor3f(0.6,0.6,0.6);
        /*else if(cells_[n].fakeObstacle)
            glColor3f(0.0,0.0,1.0);*/

        /*else if (cells_[n].laserLimit)
            glColor3f(0.0,1.0,0.0);*/

//        if(cells_[n].himm_count == himm_count)
//            glColor3f(1.0,0.0,0.0);

    }else if(viewMode==2){
        if(abs(cells_[n].laser_1_count - laser_count) < 5)
            glColor3f(0.0,1.0,0.0);
        else if(abs(cells_[n].laser_2_count - laser_count) < 5)
            glColor3f(1.0,0.0,0.0);
        else{
            if(cells_[n].isObstacle)
            glColor3f(0.0,0.0,0.0);
            else if(cells_[n].isObstaclelow)
                glColor3f(0.3, 0.3, 0.3);
            else if(cells_[n].nearObstaclelow)
                glColor3f(1.0,0.0,0.0);
            else if(cells_[n].nearObstacle)
                glColor3f(0.0,0.0,1.0);   
            else if(cells_[n].visited)
                glColor3f(1.0,1.0,1.0);     
            else
                glColor3f(0.6,0.6,0.6);
        }

    }else if(viewMode==3){//mapa de alturas
        if(cells_[n].height!=UNDEF)
            aux = (cells_[n].height - minHeight)/(maxHeight - minHeight);
        glColor3f(aux,aux,aux);

    }else if(viewMode==4){//mapa de alturas minimas
        if(cells_[n].heightMin!=UNDEF)
            aux = (cells_[n].heightMin - minHeight)/(maxHeight - minHeight);
        glColor3f(aux,aux,aux);

    }else if(viewMode==5){//mapa de alturas maximas

        if(cells_[n].heightMax!=UNDEF)
            aux = (cells_[n].heightMax - minHeight)/(maxHeight - minHeight);
        
        /*else if(cells_[n].heightMinLaser!=UNDEF)
            aux = (cells_[n].heightMinLaser - minHeight)/(maxHeight - minHeight);*/
        glColor3f(aux,aux,aux);
        

    }else if(viewMode==6){//para testar
        glColor3f(1.0-cells_[n].potential,1.0-cells_[n].potential,1.0-cells_[n].potential);
    }


    glBegin( GL_QUADS );
    {
        glVertex2f(cells_[n].x+1, cells_[n].y+1);
        glVertex2f(cells_[n].x+1, cells_[n].y  );
        glVertex2f(cells_[n].x  , cells_[n].y  );
        glVertex2f(cells_[n].x  , cells_[n].y+1);
    }
    glEnd();

}

void Grid::drawVector(unsigned int n)
{
    float aux=cells_[n].potential;
    glColor3f(1.0-aux,0.0,0.0);
    glBegin( GL_LINES );
    {
        glVertex2f(cells_[n].x+0.5, cells_[n].y+0.5);
        glVertex2f(cells_[n].x+0.5+cells_[n].dirX, cells_[n].y+0.5+cells_[n].dirY);
    }
    glEnd();
    glBegin( GL_POINTS );
    {
        glVertex2f(cells_[n].x+0.5+cells_[n].dirX, cells_[n].y+0.5+cells_[n].dirY);
    }
    glEnd();
}

void Grid::drawText(unsigned int n)
{
    glRasterPos2f(cells_[n].x+0.25, cells_[n].y+0.25);
    stringstream s;
    glColor3f(0.5f, 0.0f, 0.0f);
    s << cells_[n].potential;

    string text=s.str();
    for (unsigned int i=0; i<text.size(); i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, text[i]);
    }
}
