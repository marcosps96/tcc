#ifndef __GRID_H__
#define __GRID_H__

#define UNDEF -100000

#include <vector>

class Cell
{
    public:
        int x,y;        
        bool free, visited, isObstacle, laserLimit, isObstaclelow, nearObstacle, nearObstaclelow, fakeObstacle, checked;
        int himm, himm_count, laser_1_count, laser_2_count;
        double distWalls, dirX, dirY, height, heightMin, heightMax, heightMinLaser, mean_heightMin, mean_heightMax;
        double potential, gradNorm, preference;
        std::vector<double> maxHeightsList, minHeightsList;

};

class Grid
{
    public:
        Grid();
        Cell* getCell(int x, int y);

        int getMapScale();
        int getMapWidth();
        int getMapHeight();
        void createFiles();


        void draw(int xi, int yi, int xf, int yf);

        int numViewModes;
        int viewMode;
        bool showValues;
        bool showArrows;
        double maxHeight;
        double minHeight;

        int laser_count, himm_count;

    private:
        int mapScale_; // Number of cells per meter
        int mapWidth_, mapHeight_; // in cells
        int numCellsInRow_, halfNumCellsInRow_;

        Cell* cells_;

        void drawCell(unsigned int i);
        void drawVector(unsigned int i);
        void drawText(unsigned int n);        
};

#endif // __GRID_H__
