#include "P3ATRobot.h"

#include <algorithm>
#include <tf/transform_datatypes.h>
#include "Utils.h"
#include <queue>    

using namespace std;

P3ATRobot::P3ATRobot(int argc, char** argv): Robot()
{
    ros::init(argc, argv, "control_P3AT");

    ROS_INFO("control_P3AT");

    n_ = new ros::NodeHandle("~");
    rate_ = new ros::Rate(20);

    start = ros::WallTime::now();
    last = start;

    listener = new tf::TransformListener;

    // Initialize publishers and subscribers
    pub_twist_ = n_->advertise<geometry_msgs::Twist>("/sim_p3at/cmd_vel", 1);
    pub_pcl_ =  n_->advertise<sensor_msgs::PointCloud>("/cloud", 1);
    pub_pose_ =  n_->advertise<geometry_msgs::PoseWithCovarianceStamped>("/poseStamped", 1);
    sub_odom_ = n_->subscribe("/sim_p3at/odom", 100, &P3ATRobot::receiveOdom, this);
    sub_gazeboModelStates_ = n_->subscribe("/gazebo/model_states", 100, &P3ATRobot::receiveTruePose, this);
    sub_laser_ = n_->subscribe("/scan", 100, &P3ATRobot::receiveLaser, this);
    sub_laser_2_= n_->subscribe("/scan_2", 100, &P3ATRobot::receiveLaser_2, this);
    started_exploration = false;

}

P3ATRobot::~P3ATRobot()
{
    if(grid!=NULL)
        delete grid;
}

void P3ATRobot::run()
{
    readOdometryAndSensors();

    updatePoints();

    updateLaserData(cloud_);
    updateLaserData(cloud_2_);

    updateObstacles(cloud_, cloudObstacle_, 1);
    updateObstacles(cloud_2_, cloudObstacle_2_, 2);

    //setPreferences(cloud_, cloudObstacle_);
    //setPreferences(cloud_2_, cloudObstacle_2_);

    windPotential();

    updateGrid();

    // Navigation
    switch(motionMode_){
        case ENDING:
            move(STOP);
            pub_twist_.publish(twistROS_);
            running_=false;
            break;
        case FOLLOWPOT:
            followPotentialField();
            break;
        default:
            break;
    }
    // Get current time
    current = ros::WallTime::now();
    //cout << "RUNNING P3AT ROBOT - t:" << current-start << endl;

    // Update robot motion
    pub_twist_.publish(twistROS_);

    ros::spinOnce();
    rate_->sleep();
}

void P3ATRobot::move(MovingDirection dir)
{
    //Twist twistROS
        //# This expresses velocity in free space broken into its linear and angular parts.
        //Vector3  linear
            //float64 x
            //float64 y
            //float64 z
        //Vector3  angular
            //float64 x
            //float64 y
            //float64 z
    path_.push_back(truePose_);

    switch(dir){
        case FRONT:
            cout << "moving front" << endl;
            twistROS_.linear.x += 0.05;
            break;
        case BACK:
            cout << "moving back" << endl;
            twistROS_.linear.x -= 0.05;
            break;
        case LEFT:
            cout << "turning left" << endl;
            twistROS_.angular.z += DEG2RAD(7);
            break;
        case RIGHT:
            cout << "turning right" << endl;
            twistROS_.angular.z -= DEG2RAD(7);
            break;
        case STOP:
            cout << "stopping robot" << endl;
            twistROS_.linear.x = 0;
            twistROS_.angular.z = 0;
    }

    if(twistROS_.linear.x > 1.0)
        twistROS_.linear.x = 1.0;
    else if(twistROS_.linear.x < -1.0)
        twistROS_.linear.x = -1.0;

    if(twistROS_.angular.z > DEG2RAD(90.0))
        twistROS_.angular.z = DEG2RAD(90.0);
    else if(twistROS_.angular.z < DEG2RAD(-90.0))
        twistROS_.angular.z = DEG2RAD(-90.0);
}

void P3ATRobot::followPotentialField()
{
    int scale = grid->getMapScale();
    int robotX = truePose_.x*scale;
    int robotY = truePose_.y*scale;
    float robotAngle = truePose_.theta;

    Cell* c=grid->getCell(robotX,robotY);
    double x = c->dirX;
    double y = c->dirY;
    //cout << "FOLLOWPOT dirX " << x << " dirY " << y << endl;
    path_.push_back(truePose_);

    if(isExplorationComplete()){        
        twistROS_.linear.x = 0.0;
        twistROS_.angular.z = 0;
        motionMode_ = MANUAL;
        return;
    }
    else if(x == 0.0 && y == 0.0 && !started_exploration){
        twistROS_.linear.x = 0.15;
        twistROS_.angular.z = 0;
        return;
    }
    else if(x == 0.0 && y == 0.0 && started_exploration){
        twistROS_.linear.x = 0.0;
        twistROS_.angular.z = 0;
        return;
    }
    else{
        float phi = RAD2DEG(atan2(y,x)) - robotAngle;
        started_exploration = true;
        phi = normalizeAngleDEG(phi);
        twistROS_.linear.x = 0.15;//(leftWheel + rightWheel)/10.0;
        twistROS_.angular.z = 0.0025*phi;//5.0*(leftWheel-rightWheel);
    }

}

bool P3ATRobot::isExplorationComplete(){

    Cell* c;    
    int robotX = truePose_.x*grid->getMapScale();
    int robotY = truePose_.y*grid->getMapScale();
    int offset[4][2];
    offset[0][0] = 1;
    offset[1][0] = 1;
    offset[2][0] = -1;
    offset[3][0] = -1;

    offset[0][1] = 1;
    offset[1][1] = -1;
    offset[2][1] = 1;
    offset[3][1] = -1;

    c = grid->getCell(robotX, robotY);

    bool validPosition = false;
    while(!validPosition){
        if(c->visited && c->free && !c->nearObstaclelow && !c->nearObstacle)
            validPosition = true;
        else{
            robotX++;
            c = grid->getCell(robotX, robotY);
        }
    }

    for(int i=-400; i<=400; i++){
        for(int j=-400; j<=400; j++){
            c = grid->getCell(i,j);
            c->checked = false;
        }
    }


    std::queue<Cell*> q;
    c = grid->getCell(robotX, robotY);
    c->checked = true;
    q.push(c);

    while(!q.empty()){
        Cell* c = q.front();
        q.pop();

        if(!c->visited){
            return false;
        }else{
            for(int k=0; k<4; k++){
                Cell* n = grid->getCell(c->x+offset[k][0],c->y+offset[k][1]);

                if( ( (!n->isObstacle && !n->nearObstacle && !n->isObstaclelow && !n->nearObstaclelow) || !n->visited) && n->checked == false){
                    n->checked = true;
                    q.push(n);
                }
            }
        }
    }
    return true;
}

void P3ATRobot::setPreferences(sensor_msgs::PointCloud &cloud, vector<bool> cloudObstacle){

    Cell *c, *robotCell;

    int robotX = truePose_.x*grid->getMapScale();
    int robotY = truePose_.y*grid->getMapScale();

    robotCell = grid->getCell(robotX, robotY);

    //for(int a = 0; a < cloud.points.size(); a++){
        //if(cloudObstacle[a]){
    for(int i = -200; i <= 200; i++)
        for(int j = -200; j <= 200; j++){
            //int i = cloud.points[a].x*grid->getMapScale();
            //int j = cloud.points[a].y*grid->getMapScale();

            c = grid->getCell(i,j);

            if(robotCell->height != UNDEF && c->height != UNDEF)
                if(fabs(robotCell->heightMax - c->heightMax) > 0.05)
                    c->preference = -0.12;
                else c->preference = 0.0;
        }
    //}
}

void P3ATRobot::updateObstacles(sensor_msgs::PointCloud &cloud, vector<bool> cloudObstacle, int laser_id){

    Cell *d, *c, *n, *robotCell;
    Cell *surrounding_c;
    float neighbourhood_max = 0;
    float neighbourhood_min = 0;
    float heightMax_diff = 0;
    float heightMin_diff = 0;
    float lineHeight = 0;
    float count = 0;
    float total = 0;
    float dist = 0;
    int robotX = truePose_.x*grid->getMapScale();
    int robotY = truePose_.y*grid->getMapScale();
    int robotZ = truePoseROS_.pose.orientation.z+0.4;
    float robotAngle = truePose_.theta;

    //cout << laserROS_.range_max << endl;

    //definir obstáculos a partir da diferença de altura em relação as células vizinhas
    for(int a = 0; a < cloud.points.size(); a++){
        if(cloudObstacle[a]){          
            int i = cloud.points[a].x*grid->getMapScale();
            int j = cloud.points[a].y*grid->getMapScale();

            c = grid->getCell(i,j);

            neighbourhood_max = 0;
            neighbourhood_min = 0;
            count = 0;
            total = 0;
            //dist = distance(robotX, robotY, i, j);
            //cout << dist << " " << distance(-150,-150, 150, 150) << endl;

            if(c->visited){ // && i >= robotX - 100 && i <= robotX + 100 && j >= robotY - 100 && j <= robotY + 100){
                for(int surrounding_i = i-20; surrounding_i <= i+20; surrounding_i++)
                    for(int surrounding_j = j-20; surrounding_j <= j+20; surrounding_j++){
                        if(1==1){//surrounding_i >= -999 && surrounding_i < 1000 && surrounding_j >= -999 && surrounding_j < 1000){
                            if(surrounding_i != i || surrounding_j != j){
                                surrounding_c = grid->getCell(surrounding_i, surrounding_j);

                                if (surrounding_c->visited && surrounding_c->heightMax != UNDEF && surrounding_c->heightMin){
                                    neighbourhood_max += surrounding_c->heightMax;
                                    neighbourhood_min += surrounding_c->heightMin;
                                    count++;
                                }
                                total++;
                            }
                        }
                    }

                if(1==1){//count/total > 0.35){ //checar efeito disso
                    neighbourhood_max /= count;
                    neighbourhood_min /= count;

                    if ((c->heightMax != UNDEF && c->heightMin != UNDEF)){// && (fabs(c->heightMax - meanHeight) > 0.30)){ //testar abs()
                        heightMax_diff = c->heightMax - neighbourhood_max;
                        heightMin_diff = c->heightMin - neighbourhood_min;

                        if(heightMax_diff >= 0 && heightMax_diff > 0.3 && c->free && !c->nearObstaclelow && count/total > 0.15){
                            c->isObstacle = true;
                            c->free = false;
                        }
                        else if(heightMin_diff < 0 && heightMin_diff < -0.6 && c->free && !c->nearObstacle && count/total > 0.35){
                            c->isObstaclelow = true;
                            c->free = false;
                        }
                        //cout << "count " << count << " cell height: " << c->height << " mean height: " << meanHeight << endl;
                        //c->isObstacle = true;
                    }
                }
            } 
        }
    }

    int width=3;
    int width_low=3;
    //fill holes
    if(laser_id == 1){
        int half = cloud.points.size()/2;
        if (half>0){
            for(int a = half-225; a <= half+225; a++){
                int i = cloud.points[a].x*grid->getMapScale();
                int j = cloud.points[a].y*grid->getMapScale();
                if (cloudObstacle[a]){
                    robotCell = grid->getCell(robotX, robotY);
                    c = grid->getCell(i,j);
                    dist = distance(robotX, robotY, i, j);

                    if((c->free && c->visited && c->heightMin != UNDEF && robotCell->heightMin != UNDEF) && c->heightMin - robotCell->heightMin < -0.8 && dist < 100){
                        c->isObstaclelow = true;
                        if(i > robotX){
                            for(int x = i-1; x >= robotX+5; x--){
                                n = grid->getCell(x, j);
                                if(n->height != UNDEF){
                                    if(j > robotY){
                                        for(int y = j-1; y >= robotY; y--){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true;
                                                d->free = false;
                                            }
                                            else if(d->height != UNDEF)
                                                break;
                                        }
                                    }
                                    else{
                                        for(int y = j+1; y <= robotY; y++){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true;
                                                d->free = false;
                                            }
                                            else if(d->height != UNDEF)
                                                break;                                                                 
                                        }
                                    }
                                }
                            }
                        }

                        else{
                            for(int x = i+1; x <= robotX-5; x++){
                                n = grid->getCell(x, j);
                                if(n->height != UNDEF){
                                    if(j > robotY){
                                        for(int y = j-1; y >= robotY; y--){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true;
                                                d->free = false;
                                            }
                                            else if(d->height != UNDEF)
                                                break;
                                        }
                                    }
                                    else{
                                        for(int y = j+1; y <= robotY; y++){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true;
                                                d->free = false;
                                            }
                                            else if(d->height != UNDEF)
                                                break;                                                                          
                                        }
                                    }
                                }
                            }
                        }

                        if(j > robotY){
                            for(int y = j-1; y >= robotY+5; y--){
                                n = grid->getCell(i, y);
                                if(n->height != UNDEF){
                                    if(i > robotX){
                                        for(int x = i-1; x >= robotX; x--){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true;
                                                d->free = false;
                                            }
                                            else if(d->height != UNDEF)
                                                break;
                                        }
                                    }
                                    else{
                                        for(int x = i+1; x <= robotX; x++){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true;
                                                break;
                                            }
                                            else if(d->height != UNDEF)
                                                break;                                                               
                                        }
                                    }
                                }
                            }
                        }

                        else{
                            for(int y = j+1; y <= robotY-5; y++){
                                n = grid->getCell(i, y);
                                if(n->height != UNDEF){
                                    if(i > robotX){
                                        for(int x = i-1; x >= robotX; x--){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true; 
                                                d->free = false;
                                            }
                                            else if(d->height != UNDEF)
                                                break;
                                        }
                                    }
                                    else{
                                        for(int x = i+1; x <= robotX; x++){
                                            d = grid->getCell(x,y);
                                            if(d->height == UNDEF && d->free && !d->nearObstacle){
                                                d->isObstaclelow = true;
                                                d->free = false;
                                            }
                                            else if(d->height != UNDEF)
                                                break;                                                                         
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    //expand obstacles

    for(int i = -200; i <= 200; i++){
        for(int j = -200; j <= 200; j++){    
    /*for(int a = 0; a < cloud.points.size(); a++){
        if(cloudObstacle[a]){
            int i = cloud.points[a].x*grid->getMapScale();
            int j = cloud.points[a].y*grid->getMapScale();*/    
            c = grid->getCell(i,j);

            if(c->isObstacle){
                for(int x=i-width;x<=i+width;x++){
                    for(int y=j-width;y<=j+width;y++){
                        n = grid->getCell(x,y);
                        if(!n->isObstacle){
                            n->nearObstacle = true;
                            //n->free = false;
                        }
                    }
                }
            }
            else if(c->isObstaclelow){
                for(int x=i-width_low;x<=i+width_low;x++){
                    for(int y=j-width_low;y<=j+width_low;y++){
                        n = grid->getCell(x,y);
                        if(!n->isObstacle && !n->isObstaclelow && !n->nearObstacle){
                            n->nearObstaclelow = true;
                            //n->free = false;
                        }
                    }
                }
            }

        }
    }


}
/*void P3ATRobot::update_minHeightsList(double minHeight, Cell* c){
    c->heightMin = 0;
    double list_mean = 0;
    int count = 0;
    bool found = false;

    if (c->minHeightsList.size() < 3)
        c->minHeightsList.push_back(minHeight);
    else{
        int i = 0;
        while(!found){
            if(minHeight < c->minHeightsList[i]){
                found = true;
                c->minHeightsList[c->minHeightsList.size()-1] = minHeight;
            }
        }
    }

    sort(c->minHeightsList.begin(), c->minHeightsList.end());

    for(int i=0; i < c->minHeightsList.size(); i++){
        list_mean += c->minHeightsList[i];
    }
    list_mean /= c->minHeightsList.size();

    for(int i=0; i < c->minHeightsList.size(); i++){
        if(c->minHeightsList[i] - list_mean <= 0.3){
            count++;
            c->heightMin += c->minHeightsList[i];
        }  
    }
    c->heightMin /= count;

}

void P3ATRobot::update_maxHeightsList(double maxHeight, Cell* c){
    c->heightMax = 0;
    double list_mean = 0;
    int count = 0;
    bool found = false;

    if (c->maxHeightsList.size() < 2)
        c->maxHeightsList.push_back(maxHeight);
    else{
        int i = 0;
        while(!found){
            if(maxHeight > c->maxHeightsList[i]){
                found = true;
                c->maxHeightsList[0] = maxHeight;
            }
        }
    }

    sort(c->maxHeightsList.begin(), c->maxHeightsList.end());

    for(int i=0; i < c->maxHeightsList.size(); i++){
        list_mean += c->maxHeightsList[i];
    }
    list_mean /= c->maxHeightsList.size();

    for(int i=0; i < c->maxHeightsList.size(); i++){
        if(c->maxHeightsList[i] - list_mean <= 0.3){           
            count++;
            c->heightMax += c->maxHeightsList[i];
        }  
    }
    c->heightMax /= count;

}*/

void P3ATRobot::updatePoints(){
    Cell* c;
    int scale = grid->getMapScale();

    int robotX = truePose_.x*scale;
    int robotY = truePose_.y*scale;
    float robotZ = truePoseROS_.pose.orientation.z+0.4; //?? (sempre 0.4)
    //c = grid->getCell(robotX, robotY);
    //c->height = robotZ;

    if(robotZ < grid->minHeight)
        grid->minHeight = robotZ;
    if(robotZ > grid->maxHeight)
        grid->maxHeight = robotZ;

    grid->laser_count++;

    //laser_1
    for(int i = 0; i < cloud_.points.size(); i++){
        if(cloudObstacle_[i]){
            c = grid->getCell(cloud_.points[i].x*scale, cloud_.points[i].y*scale);
            c->laser_1_count = grid->laser_count;
            c->height = cloud_.points[i].z; //atualiza o mapa de alturas
            
            if(c->heightMax == UNDEF || c->heightMax < cloud_.points[i].z){ //atualiza o mapa de alturas maximas
                c->heightMax = cloud_.points[i].z;       
                //update_maxHeightsList(cloud_.points[i].z, c);
            }  

            if(c->heightMin == UNDEF || c->heightMin > cloud_.points[i].z){ //atualiza o mapa de alturas minimas
                c->heightMin = cloud_.points[i].z;
                //update_minHeightsList(cloud_.points[i].z, c);
            }
            
            if(c->height < grid->minHeight)
                grid->minHeight = c->height;
            if(c->height > grid->maxHeight)
                grid->maxHeight = c->height;
        }

    }

    //laser_2 laser reto
    for(int i = 0; i < cloud_2_.points.size(); i++){
        if(cloudObstacle_2_[i]){
            c = grid->getCell(cloud_2_.points[i].x*scale, cloud_2_.points[i].y*scale);
            c->laser_2_count = grid->laser_count;
            c->height = cloud_2_.points[i].z; //atualiza o mapa de alturas

            if(c->heightMax == UNDEF || c->heightMax < cloud_2_.points[i].z){ //atualiza o mapa de alturas maximas
                c->heightMax = cloud_2_.points[i].z;
                //update_maxHeightsList(cloud_2_.points[i].z, c);
            }

            if(c->heightMin == UNDEF || c->heightMin > cloud_2_.points[i].z){ //atualiza o mapa de alturas minimas
                c->heightMin = cloud_2_.points[i].z;                
                //update_minHeightsList(cloud_2_.points[i].z, c);
            }


            if(c->height < grid->minHeight)
                grid->minHeight = c->height;
            if(c->height > grid->maxHeight)
                grid->maxHeight = c->height;
        }

    }

}

void P3ATRobot::updateLaserData(sensor_msgs::PointCloud &cloud)
{
    int scale = grid->getMapScale();
    int robotX = truePose_.x*scale;
    int robotY = truePose_.y*scale;
    float robotZ = truePoseROS_.pose.orientation.z +0.4;

    float robotAngle = DEG2RAD(truePose_.theta);

    float radiusVisited = 12.0*scale; //importante!

    laserData_.resize(cloud.points.size());
    float minAngle = 5000.0;
    float maxAngle = -5000.0;

    for(int i=0; i<cloud.points.size();i++)//armazena os dados da cloud_1 no vetor de medidas do laser
    {
        float laserX = cloud.points[i].x*scale;
        float laserY = cloud.points[i].y*scale;        
        float alpha = atan2(laserY-robotY,laserX-robotX);
        float r = sqrt(pow(robotX-laserX,2)+pow(robotY-laserY,2));

        laserData_[i].alpha_ = normalizeRAD(alpha - robotAngle);
        laserData_[i].r_ = r;
        laserData_[i].x_ = laserX;
        laserData_[i].y_ = laserY;
        laserData_[i].z_ = cloud.points[i].z;

        minAngle = min(minAngle,laserData_[i].alpha_ );
        maxAngle = max(maxAngle,laserData_[i].alpha_ );
    }
    //cout << "robotAngle " << RAD2DEG(robotAngle) << " minAngle " << RAD2DEG(minAngle) << " maxAngle " << RAD2DEG(maxAngle) << endl;

    Cell* c;
    float maxCellsRange = laserROS_2_.range_max*scale;

    sort(laserData_.begin(),laserData_.end());

    for(int i=robotX-maxCellsRange; i<=robotX+maxCellsRange; i++)
        for(int j=robotY-maxCellsRange; j<=robotY+maxCellsRange; j++)
        {
            float vectRobotX = i - robotX;
            float vectRobotY = j - robotY;
            float radiusCell = sqrt(pow(vectRobotX,2)+pow(vectRobotY,2));
            float cellAngle = atan2(vectRobotY,vectRobotX);
            float diffAngle = cellAngle - robotAngle;
            diffAngle = normalizeRAD(diffAngle);

            if(diffAngle >= minAngle && diffAngle <= maxAngle){
                LaserMeasurements value;
                value.alpha_ = diffAngle;

                std::vector<LaserMeasurements>::iterator low;
                low = lower_bound (laserData_.begin(),laserData_.end(), value);

                const LaserMeasurements& before = *low;
                const LaserMeasurements& after = *(low++);
//                cout << "Before: " << before.alpha_ << ' ' << before.r_
//                     << " After: " << after.alpha_  << ' ' << after.r_  <<   endl;
                if(fabs(diffAngle-before.alpha_)<fabs(diffAngle-after.alpha_))
                    value = before;
                else
                    value = after;

                if(radiusCell <= value.r_ && radiusCell <= radiusVisited){
                    c = grid->getCell(i,j);
                    c->visited = true;

                    // (zcell - zrobot) / (zlaser - zrobot) = (raio cell) / raio laser
                    // zcell - zrobot = (zlaser - zrobot) *  (raio cell) / raio laser
                    // zcell = zrobot + (zlaser - zrobot) *  (raio cell) / raio laser

                    float heightLaserOverCell = robotZ + (value.z_-robotZ) * (radiusCell/value.r_);
                    if(c->heightMinLaser == UNDEF){ 
                        c->heightMinLaser = heightLaserOverCell;
                    }else if(c->heightMinLaser > heightLaserOverCell){
                        c->heightMinLaser = heightLaserOverCell;
                    }
                }
            }
        }

    c = grid->getCell(robotX,robotY);
    c->visited = true;
}

/* calcular a windPotential na classe potencial
 * setar i minX e minY do potencial por aqui
 * pegar o raio de um obstaculo para determinar o tamanhoa da janela  */

void P3ATRobot::windPotential(){//mudar o nome ou fazer isso em outra

    Cell* c;
    int scale = grid->getMapScale();

    int robotX = truePose_.x*scale;
    int robotY = truePose_.y*scale;
    float robotAngle = DEG2RAD(truePose_.theta);
    float fakeObstacleAngle = truePose_.theta + 180; //theta + 180º

    float r = 1.2*laserROS_.range_max*scale;
    r = 180;//300;//12*scale; //parece funcionar melhor (robo da menos loops) 
    minX_ = robotX - r;
    minY_ = robotY - r;
    maxX_ = robotX + r;
    maxY_ = robotY + r;

    /*
    for (int i = 0; i < fakeObstacle.size(); i++){
        int x = fakeObstacle[i].first;
        int y = fakeObstacle[i].second;

        c = grid->getCell(x,y);
        c->fakeObstacle = false;
    }
    if(robotAngle >= -M_PI*2 && robotAngle <= M_PI*2){ // avoid NaN
        fakeObstacle.clear();

        int cx = robotX;
        int cy = robotY;

        int r = 10; //testar raios

        for(int i = fakeObstacleAngle-60; i <= fakeObstacleAngle+60;i++){
            float a = DEG2RAD(i);

            float x = cx + r*cos(a);
            float y = cy + r*sin(a);

            c = grid->getCell(x,y);
            //c->fakeObstacle = true;

            pair<int,int> pairXY = make_pair(x,y);
            fakeObstacle.push_back(pairXY);

            float x2 = cx + (r-1)*cos(a);
            float y2 = cy + (r-1)*sin(a);

            c = grid->getCell(x2,y2);
            //c->fakeObstacle = true;

            pair<int,int> pairXY2 = make_pair(x2,y2);
            fakeObstacle.push_back(pairXY2);            
        }
        for(int i = robotX - 5; i < robotX + 5; i++){

            double x1 = i - robotX;
            double y1 = (robotY-5) - robotY;

            double x2 = x1 * cos(robotAngle) - y1 * sin(robotAngle);
            double y2 = x1 * sin(robotAngle) + y1 * cos(robotAngle);


            c = grid->getCell(x2 + robotX, y2 + robotY);
            //c = grid->getCell(i, robotY-5);
            
            c->fakeObstacle = true;
        }
    }*/

    /*int windRange = 50;//esse valor vai ser calculado a partir de um obstaculo
    minX_ = robotX - windRange;
    minY_ = robotY - windRange;
    maxX_ = robotX + windRange;
    maxY_ = robotY + windRange;

    for(int i=robotX - windRange; i<=robotX + windRange; i++)
        for(int j=robotY - windRange; j<= robotY + windRange; j++)
        {
            c = grid->getCell(i,j);
            c->isObstacle = false;
        }

    for(int i=robotX - windRange; i<=robotX + windRange; i++)
        for(int j=robotY - windRange; j<= robotY + windRange; j++)
        {
            if(i  == robotX - windRange)
            {
                c = grid->getCell(i,j);
                c->isObstacle = true;
            }
            if(i == robotX + windRange)
            {
                c = grid->getCell(i,j);
                c->isObstacle = true;
            }
            if(j == robotY - windRange)
            {
                c = grid->getCell(i,j);
                c->isObstacle = true;
            }
            if(j == robotY + windRange)
            {
                c = grid->getCell(i,j);
                c->isObstacle = true;
            }
        }*/
}

//checar essa função
void P3ATRobot::updateGrid(){ // Essa função está implementando o HIMM
    Cell* c;
    int scale = grid->getMapScale();
    float maxCellsRange = laserROS_.range_max*scale;
    maxCellsRange = 50;

    float robotAngle = DEG2RAD(truePose_.theta);
    robotAngle = normalizeRAD(robotAngle);

    int robotX = truePose_.x*scale;
    int robotY = truePose_.y*scale;

    grid->himm_count++;

    /*for(int i=0; i<laserROS_.ranges.size(); i += 1){
        float laserAngle = robotAngle + laserROS_.angle_min + i*laserROS_.angle_increment;


        float val = laserROS_.ranges[i];
        float displacX = cos(laserAngle)*val*scale;
        float displacY = sin(laserAngle)*val*scale;

//        cout << i << ": " << laserAngle*180.0/M_PI << '|' << laserROS_.ranges[i] << ' ';

        float displacResultX = robotX + displacX;
        float displacResultY = robotY + displacY;

        //cout << displacResultX << ',' << displacResultY << ' ';

        int w=1;
        for(int i=displacResultX-w; i<=displacResultX+w;i++)
            for(int j=displacResultY-w; j<=displacResultY+w;j++)
            {
                //pinta a borda do laser
                c = grid->getCell(i,j);
                c->laserLimit = true;
                c->himm_count = grid->himm_count;
            }
    }*/
    //cout << endl;

    for(int i=robotX-maxCellsRange; i<=robotX+maxCellsRange; i++)
        for(int j=robotY-maxCellsRange; j<=robotY+maxCellsRange; j++)
        {
            //pinta onde não está livre
            float vectRobotX = i - robotX;
            float vectRobotY = j - robotY;

            float r = sqrt(vectRobotX*vectRobotX + vectRobotY*vectRobotY); // r

            float phi = atan2(vectRobotY,vectRobotX) - robotAngle;
            phi = normalizeRAD(phi); //phi

            float minAngle = normalizeRAD(laserROS_.angle_min);
            float maxAngle = normalizeRAD(laserROS_.angle_max);
            

            if(phi >= minAngle && phi <= maxAngle) {

                //inverse sensor model
                int laserID = (phi-minAngle)/(maxAngle-minAngle)*(laserROS_.ranges.size());
                float sensorReading = laserROS_.ranges[laserID]*scale; //sensor reading
                float sensorAngle = laserID * laserROS_.angle_increment; // CHECAR SE ISSO TÁ CERTO
                sensorAngle = normalizeRAD(sensorAngle);

                c = grid->getCell(i,j);

                //cout << (laserROS_.angle_max - laserROS_.angle_min)/laserROS_.angle_increment << endl << laserID << endl;
                
                //cout << endl << endl;
                if(r > std::min(maxCellsRange, float(sensorReading +0.05)) || fabs(phi - sensorAngle) > 3) {
                    if(c->himm <= 13)
                        c->himm += -1;
                    //unknown region

                    //cout << "range size: " << laserROS_.ranges.size() << endl << "id: " << laserID << endl;
                    //cout << "R: " << r << endl << "PHI: " << phi << endl << "Sensor Reading: " << sensorReading << endl;
                    //cout << "Sensor Angle: " << sensorAngle << endl << "Max range: " << maxCellsRange << endl;
                }
                else if((sensorReading < maxCellsRange) && fabs(r - sensorReading) < 2){
                    if(c->himm <= 13)
                        c->himm += 3;
                    //occupied
                }
                else if(r <= sensorReading){
                    if(c->himm > 0)
                        c->himm -=1;
                    //free
                }
                //inverse sensor model

                /*
                int laserID = (diffAngle//phi-laserROS_.angle_min)/(laserROS_.angle_max-laserROS_.angle_min)*(laserROS_.ranges.size()-1);
                float normVectLaser = laserROS_.ranges[laserID]*scale; //sensor reading
                if(normVectRobot//r <= normVectLaser){
                    c = grid->getCell(i,j);
                    c->himm = 8;
                }
                */
            }
        }
}

void P3ATRobot::readOdometryAndSensors()
{
    odometry_.x = odomROS_.pose.pose.position.x;
    odometry_.y = odomROS_.pose.pose.position.y;

    tf::Quaternion q1(odomROS_.pose.pose.orientation.x,
                     odomROS_.pose.pose.orientation.y,
                     odomROS_.pose.pose.orientation.z,
                     odomROS_.pose.pose.orientation.w);
    tf::Matrix3x3 m1(q1);
    double roll, pitch, yaw;
    m1.getRPY(roll,pitch,yaw);

    odometry_.theta = RAD2DEG(yaw);

    //cout << "ODOM: " << odometry_ << endl;

    truePose_.x = truePoseROS_.pose.position.x;
    truePose_.y = truePoseROS_.pose.position.y;

    tf::Quaternion q2(truePoseROS_.pose.orientation.x,
                     truePoseROS_.pose.orientation.y,
                     truePoseROS_.pose.orientation.z,
                     truePoseROS_.pose.orientation.w);
    tf::Matrix3x3 m2(q2);
    m2.getRPY(roll,pitch,yaw);

    truePose_.theta = RAD2DEG(yaw);

    //cout << "TRUEPOSE: " << truePose_ << endl;

    //cout << "LASER: " << laserROS_.ranges.size() << endl;

    // Convert laser to point cloud
    laserToCloud();

}

void P3ATRobot::laserToCloud(){

    //laser max range is 30 (30*scale = 300), using 12 to limit
    // Convert laser to point cloud_ use laser_1
    int scale = grid->getMapScale();
    projector_.projectLaser(laserROS_, cloud_);
    pub_pcl_.publish(cloud_);

    cloudObstacle_.resize(cloud_.points.size());
    for(int c=0; c<cloud_.points.size(); c++){
        if(sqrt(pow(cloud_.points[c].x,2.0) + pow(cloud_.points[c].y,2.0)) < 0.4*laserROS_.range_max)
            cloudObstacle_[c] = true;
        else
            cloudObstacle_[c] = false;
    }

    transformCloud(cloud_, "/sick_lms100");

    // Convert laser to point cloud_2 use laser_2

    projector_.projectLaser(laserROS_2_, cloud_2_);
    pub_pcl_.publish(cloud_2_);

    cloudObstacle_2_.resize(cloud_2_.points.size());
    for(int c=0; c<cloud_2_.points.size(); c++){
        if(sqrt(pow(cloud_2_.points[c].x,2.0) + pow(cloud_2_.points[c].y,2.0)) < 0.4*laserROS_2_.range_max) // 0.4 * 30 = 12
            cloudObstacle_2_[c] = true;
        else
            cloudObstacle_2_[c] = false;
    }

    transformCloud(cloud_2_, "/sick_lms100_2");
}

void P3ATRobot::receiveOdom(const nav_msgs::Odometry::ConstPtr &value)
{

//  STRUCTURE OF nav_msgs::Odometry

//# This represents an estimate of a position and velocity in free space.
//# The pose in this message should be specified in the coordinate frame given by header.frame_id.
//# The twist in this message should be specified in the coordinate frame given by the child_frame_id

    //Header header
    //    # Standard metadata for higher-level stamped data types.
    //    # This is generally used to communicate timestamped data
    //    # in a particular coordinate frame.
    //    #
    //    # sequence ID: consecutively increasing ID
    //    uint32 seq
    //    #Two-integer timestamp that is expressed as:
    //    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    //    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    //    # time-handling sugar is provided by the client library
    //    time stamp
    //    #Frame this data is associated with
    //    # 0: no frame
    //    # 1: global frame
    //    string frame_id
    odomROS_.header = value->header;

    //string child_frame_id
    odomROS_.child_frame_id = value->child_frame_id;

    //geometry_msgs/PoseWithCovariance pose
        //# This represents a pose in free space with uncertainty.
        //Pose pose
            //# A representation of pose in free space, composed of position and orientation.
            //Point position
                //# This contains the position of a point in free space
                //float64 x
                //float64 y
                //float64 z
            //Quaternion orientation
                //# This represents an orientation in free space in quaternion form.
                //float64 x
                //float64 y
                //float64 z
                //float64 w
        //# Row-major representation of the 6x6 covariance matrix
        //# The orientation parameters use a fixed-axis representation.
        //# In order, the parameters are:
        //# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
        //float64[36] covariance
    odomROS_.pose = value->pose;

    //geometry_msgs/TwistWithCovariance twist
        //# This expresses velocity in free space with uncertainty.
        //Twist twist
            //# This expresses velocity in free space broken into its linear and angular parts.
            //Vector3  linear
                //float64 x
                //float64 y
                //float64 z
            //Vector3  angular
                //float64 x
                //float64 y
                //float64 z
        //# Row-major representation of the 6x6 covariance matrix
        //# The orientation parameters use a fixed-axis representation.
        //# In order, the parameters are:
        //# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
        //float64[36] covariance
    odomROS_.twist = value->twist;

    // Publish poseStamped
    geometry_msgs::PoseWithCovarianceStamped p;
    p.header = value->header;
    p.pose = value->pose;
    pub_pose_.publish(p);
}

void P3ATRobot::receiveTruePose(const gazebo_msgs::ModelStates::ConstPtr &value)
{
//    STRUCTURE OF gazebo_msgs::ModelStates

    //# broadcast all model states in world frame
    //string[] name                 # model names
    int id = value->name.size()-1; // pioneer3at_robot is the last object in the gazebo model_states
    truePoseROS_.model_name = value->name[id];

    //geometry_msgs/Pose[] pose     # desired pose in world frame
        //# A representation of pose in free space, composed of position and orientation.
        //Point position
            //# This contains the position of a point in free space
            //float64 x
            //float64 y
            //float64 z
        //Quaternion orientation
            //# This represents an orientation in free space in quaternion form.
            //float64 x
            //float64 y
            //float64 z
            //float64 w
    truePoseROS_.pose = value->pose[id];

    //geometry_msgs/Twist[] twist   # desired twist in world frame
        //# This expresses velocity in free space broken into its linear and angular parts.
        //Vector3  linear
            //float64 x
            //float64 y
            //float64 z
        //Vector3  angular
            //float64 x
            //float64 y
            //float64 z
    truePoseROS_.twist = value->twist[id];
}

void P3ATRobot::receiveLaser(const sensor_msgs::LaserScan::ConstPtr &value)
{
//  STRUCTURE OF sensor_msgs::LaserScan

    //Header header
    //    # Standard metadata for higher-level stamped data types.
    //    # This is generally used to communicate timestamped data
    //    # in a particular coordinate frame.
    //    #
    //    # sequence ID: consecutively increasing ID
    //    uint32 seq
    //    #Two-integer timestamp that is expressed as:
    //    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    //    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    //    # time-handling sugar is provided by the client library
    //    time stamp
    //    #Frame this data is associated with
    //    # 0: no frame
    //    # 1: global frame
    //    string frame_id
    //             # timestamp in the header is the acquisition time of
    //             # the first ray in the scan.
    //             #
    //             # in frame frame_id, angles are measured around
    //             # the positive Z axis (counterclockwise, if Z is up)
    //             # with zero angle being forward along the x axis
    laserROS_.header = value->header;

    //float32 angle_min        # start angle of the scan [rad]
    //float32 angle_max        # end angle of the scan [rad]
    //float32 angle_increment  # angular distance between measurements [rad]
    laserROS_.angle_min = value->angle_min;
    laserROS_.angle_max = value->angle_max;
    laserROS_.angle_increment = value->angle_increment;


    //float32 time_increment   # time between measurements [seconds] - if your scanner
    //                         # is moving, this will be used in interpolating position
    //                         # of 3d points
    //float32 scan_time        # time between scans [seconds]
    laserROS_.time_increment = value->time_increment;
    laserROS_.scan_time = value->scan_time;

    //float32 range_min        # minimum range value [m]
    //float32 range_max        # maximum range value [m]
    laserROS_.range_min = value->range_min;
    laserROS_.range_max = value->range_max;

    //float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
    //float32[] intensities    # intensity data [device-specific units].  If your
    //                         # device does not provide intensities, please leave
    //                         # the array empty.
    laserROS_.ranges = value->ranges;
    laserROS_.intensities = value->intensities;
}

void P3ATRobot::receiveLaser_2(const sensor_msgs::LaserScan::ConstPtr &value)
{
//  STRUCTURE OF sensor_msgs::LaserScan

    //Header header
    //    # Standard metadata for higher-level stamped data types.
    //    # This is generally used to communicate timestamped data
    //    # in a particular coordinate frame.
    //    #
    //    # sequence ID: consecutively increasing ID
    //    uint32 seq
    //    #Two-integer timestamp that is expressed as:
    //    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    //    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    //    # time-handling sugar is provided by the client library
    //    time stamp
    //    #Frame this data is associated with
    //    # 0: no frame
    //    # 1: global frame
    //    string frame_id
    //             # timestamp in the header is the acquisition time of
    //             # the first ray in the scan.
    //             #
    //             # in frame frame_id, angles are measured around
    //             # the positive Z axis (counterclockwise, if Z is up)
    //             # with zero angle being forward along the x axis
    laserROS_2_.header = value->header;

    //float32 angle_min        # start angle of the scan [rad]
    //float32 angle_max        # end angle of the scan [rad]
    //float32 angle_increment  # angular distance between measurements [rad]
    laserROS_2_.angle_min = value->angle_min;
    laserROS_2_.angle_max = value->angle_max;
    laserROS_2_.angle_increment = value->angle_increment;

    //float32 time_increment   # time between measurements [seconds] - if your scanner
    //                         # is moving, this will be used in interpolating position
    //                         # of 3d points
    //float32 scan_time        # time between scans [seconds]
    laserROS_2_.time_increment = value->time_increment;
    laserROS_2_.scan_time = value->scan_time;

    //float32 range_min        # minimum range value [m]
    //float32 range_max        # maximum range value [m]
    laserROS_2_.range_min = value->range_min;
    laserROS_2_.range_max = value->range_max;

    //float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
    //float32[] intensities    # intensity data [device-specific units].  If your
    //                         # device does not provide intensities, please leave
    //                         # the array empty.
    laserROS_2_.ranges = value->ranges;
    laserROS_2_.intensities = value->intensities;
}

void P3ATRobot::transformCloud(sensor_msgs::PointCloud &cloud, string laserName)
{
    tf::StampedTransform laserTransform;

    ros::Time now = ros::Time::now();
    try{
        // faster lookup transform so far
        listener->waitForTransform("/odom", laserName, now, ros::Duration(2.0)); //"/sick_lms100"
        listener->lookupTransform("/odom", laserName, now, laserTransform);
    }
    catch (tf::TransformException &ex) {
//        ROS_ERROR("%s",ex.what());
        // 30 Hz -- maybe too fast
        ros::Duration(0.05).sleep();
        return;
    }

    //fazer for para todos os pontos da nuvem pegando x,y,z
    for(int c=0; c<cloud.points.size(); c++){


        tf::Transform pointTransform;
        pointTransform.setOrigin( tf::Vector3(cloud.points[c].x,
                                              cloud.points[c].y,
                                              cloud.points[c].z) );
//            tf::Quaternion q;
//            quaternionMsgToTF(odomROS_.pose.orientation,q);
//            pointTransform.setRotation(q);

        // Compute corrected cloud pose
        tf::Transform Result;
        Result.mult(laserTransform, pointTransform);

        cloud.points[c].x = Result.getOrigin().x();
        cloud.points[c].y = Result.getOrigin().y();
        cloud.points[c].z = Result.getOrigin().z();
//        quaternionTFToMsg(Result.getRotation(),rightCoilPose_.pose.orientation);


    }

   // cout << "CLOUD " << cloud.points[100].x << ' ' << cloud.points[100].y << ' ' << cloud.points[100].z << endl;
}
