#ifndef P3ATROBOT_H
#define P3ATROBOT_H


#define UNDEF -100000

#include "Robot.h"

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>

#include <tf/transform_listener.h>
#include <tf/message_filter.h>  //poseStamped
//#include <message_filters/subscriber.h>
//#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <gazebo_msgs/ModelStates.h>
#include <gazebo_msgs/ModelState.h>
#include <laser_geometry/laser_geometry.h>

class LaserMeasurements{
public:
    float alpha_;
    float r_;
    float x_;
    float y_;
    float z_;

    LaserMeasurements(){
        alpha_ = 0.0;
        r_ = 0.0;
        x_ = 0.0;
        y_ = 0.0;
        z_ = 0.0;
    }

    bool operator< (const LaserMeasurements& right) const {
        return alpha_ < right.alpha_;
    }
};


class P3ATRobot : public Robot
{
public:
    P3ATRobot(int argc, char** argv);
    ~P3ATRobot();

    void run();
    void move(MovingDirection dir);

    const vector<geometry_msgs::PoseStamped> & getLasersPoses();

private:

    void readOdometryAndSensors();
    void updateGrid();
    void followPotentialField();

    void receiveOdom(const nav_msgs::Odometry::ConstPtr &value);
    void receiveTruePose(const gazebo_msgs::ModelStates::ConstPtr &value);
    void receiveLaser(const sensor_msgs::LaserScan::ConstPtr &value);
    void receiveLaser_2(const sensor_msgs::LaserScan::ConstPtr &value);
    void transformCloud(sensor_msgs::PointCloud &cloud, string laserName);
    void laserToCloud();
    void updatePoints();
    void updateLaserData(sensor_msgs::PointCloud &cloud);
    void windPotential();
    void updateObstacles(sensor_msgs::PointCloud &cloud, vector<bool> cloudObstacle, int laser_id);
    void update_maxHeightsList(double maxHeight, Cell* c);
    void update_minHeightsList(double minHeight, Cell* c);
    void setPreferences(sensor_msgs::PointCloud &cloud, vector<bool> cloudObstacle);
    bool isExplorationComplete();

    ros::NodeHandle* n_;
    ros::Rate* rate_;
    tf::TransformListener* listener;

    ros::WallTime start, last, current;

    nav_msgs::Odometry odomROS_;
    gazebo_msgs::ModelState truePoseROS_;
    sensor_msgs::LaserScan laserROS_;
    sensor_msgs::LaserScan laserROS_2_;
    geometry_msgs::Twist twistROS_;
    laser_geometry::LaserProjection projector_;
    sensor_msgs::PointCloud cloud_;
    sensor_msgs::PointCloud cloud_2_;
    vector<bool> cloudObstacle_;
    vector<bool> cloudObstacle_2_;
    vector< LaserMeasurements > laserData_;
    vector< LaserMeasurements > laserData_2_;
    vector< pair<int,int> > fakeObstacle; //armazena as coordenadas x,y do potencial repulsivo artificial
    bool started_exploration;

    // Ros topics subscribers
    ros::Publisher pub_twist_;
    ros::Publisher pub_pcl_;
    ros::Publisher pub_pose_;
    ros::Subscriber sub_odom_;
    ros::Subscriber sub_gazeboModelStates_;
    ros::Subscriber sub_laser_;
    ros::Subscriber sub_laser_2_;

    vector<geometry_msgs::PoseStamped> lasersPoses;
};

#endif // P3ATROBOT_H
