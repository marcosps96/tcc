
#include "Potential.h"
#include "P3ATRobot.h"


#define UNDEF -100000
//////////////////////
// Métodos Públicos //
//////////////////////
Potential::Potential()
{
    //só para inicializar
    curPref_ = 0;
    minX_=minY_= 1000;
    maxX_=maxY_= -1000;

}

Potential::~Potential()
{}

void Potential::setRobot(Robot* r)
{
    robot_=r;
}

void Potential::run()
{
    minX_ = robot_->minX_;
    minY_ = robot_->minY_;
    maxX_ = robot_->maxX_;
    maxY_ = robot_->maxY_;

    /****** Updating Traditional Potential ******/
            for(int i=0; i<100; i++)
                updateBVP();
    //        cout << "BVP!!!!!" << endl;
}

void Potential::windPotential(){

}

double Potential::updateBVP()
{
    double pot, dc, edc, prev;
    Cell *c,*l,*r,*u,*d;
    double varTotal = 0.0;
    for (int j = minY_; j <= maxY_; j++){
        for (int i = minX_; i <= maxX_; i++){            
            int tx= i;
            int ty= j;

            c=robot_->grid->getCell(tx,ty);

            if(c->isObstacle || c->isObstaclelow || c->nearObstacle || c->nearObstaclelow){
                c->potential=1.0;
            }else if(!c->visited){
                c->potential=0.0;
            }else{
                /*if (c->heightMax == UNDEF)
                    c->preference = 0.01;
                else c->preference = 0.0;*/

                prev = c->potential;
                l=robot_->grid->getCell(tx-1,ty);
                r=robot_->grid->getCell(tx+1,ty);
                u=robot_->grid->getCell(tx,ty+1);
                d=robot_->grid->getCell(tx,ty-1);

                pot = 0.25*(l->potential + r->potential + d->potential + u->potential);
                dc = fabs(u->potential - d->potential)/2 + fabs(l->potential - r->potential)/2;
                edc = c->preference*dc/4.0; //
                c->potential = pot - edc;
                varTotal += fabs(c->potential - prev);

                c->dirX = l->potential - r->potential;
                c->dirY = d->potential - u->potential;
                c->gradNorm = sqrt(c->dirX*c->dirX + c->dirY*c->dirY);
                
                if(c->gradNorm==0){
                    c->dirX = 0.0;
                    c->dirY = 0.0;
                }else{
                    c->dirX /= c->gradNorm;
                    c->dirY /= c->gradNorm;
                }
            }
        }
    }
    return varTotal;
}

