#ifndef POTENTIAL_H
#define POTENTIAL_H


class Potential;

#include "Robot.h"
#include "Grid.h"

class Potential{
   public:
        Potential();
        ~Potential();
        void setRobot(Robot* r);
        void run();
        double updateBVP();
        void windPotential();

   private:
        Robot* robot_;
        int minX_, minY_, maxX_, maxY_;
        double curPref_;

};

#endif
